import { Transaction } from '@/types';

export const transactionsList: Transaction[] = [
  {
    title: 'Cineplex Inc.',
    description: 'Sept 24, Payment',
    balance: '-$42.50',
    isDebit: true,
    isXeppt: false,
  },
  {
    title: 'RBC Royal Bank',
    description: 'Aug 1, Transfer from bank',
    balance: '+$500.00',
    isDebit: false,
    isXeppt: true,
  },
  {
    title: 'Cineplex Inc.',
    description: 'Sept 24, Payment',
    balance: '-$42.50',
    isDebit: true,
    isXeppt: false,
  },
  {
    title: 'RBC Royal Bank',
    description: 'Aug 1, Transfer from bank',
    balance: '+$500.00',
    isDebit: false,
    isXeppt: true,
  },
  {
    title: 'Cineplex Inc.',
    description: 'Sept 24, Payment',
    balance: '-$42.50',
    isDebit: true,
    isXeppt: false,
  },
  {
    title: 'RBC Royal Bank',
    description: 'Aug 1, Transfer from bank',
    balance: '+$500.00',
    isDebit: false,
    isXeppt: true,
  },
];
