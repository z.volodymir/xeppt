import { Suspense } from 'react';
import { Outlet } from 'react-router-dom';
import Container from '@/components/layout/container/Container';
import Header from '@/components/layout/header/Header';

import styles from './Layout.module.scss';

const Layout = () => {
  return (
    <div className={styles.wrapper}>
      <Header />
      <div className={styles.content}>
        <Suspense
          fallback={
            <Container>
              <p>...Loading</p>
            </Container>
          }
        >
          <Outlet />
        </Suspense>
      </div>
    </div>
  );
};

export default Layout;
