import classNames from 'classnames';

import styles from './ButtonFilter.module.scss';

interface Props {
  text: string;
  onClick: () => void;
  isActive?: boolean;
}

const ButtonFilter = ({ text, isActive = false, onClick }: Props) => {
  return (
    <button
      className={classNames(styles.button, {
        [styles.active]: isActive,
      })}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default ButtonFilter;
