import { ReactNode } from 'react';
import classNames from 'classnames';

import styles from './ButtonIcon.module.scss';

type ButtonColors = 'primary50' | 'primary80' | 'secondary30';

interface Props {
  icon: ReactNode | string;
  color: ButtonColors;
  isTransaction?: boolean;
}

const ButtonIcon = ({ icon, color, isTransaction = false }: Props) => {
  return (
    <button
      className={classNames(styles.button, {
        [styles.primary50]: color === 'primary50',
        [styles.primary80]: color === 'primary80',
        [styles.secondary30]: color === 'secondary30',
        [styles.transaction]: isTransaction,
      })}
    >
      {icon}
    </button>
  );
};

export default ButtonIcon;
