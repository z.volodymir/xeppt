import { ReactNode } from 'react';
import classNames from 'classnames';

import styles from './Title.module.scss';

interface Props {
  children: ReactNode;
  hasMargin?: boolean;
  isBank?: boolean;
}

const Title = ({ children, hasMargin = false, isBank = false }: Props) => {
  return (
    <h2
      className={classNames(styles.title, {
        [styles.margin]: hasMargin,
        [styles.bank]: isBank,
      })}
    >
      {children}
    </h2>
  );
};

export default Title;
