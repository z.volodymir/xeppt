import { useState, useRef } from 'react';
import { useClickAway } from 'react-use';
import { useMatchMedia } from '@/hooks/useMatchMedia';
import { ReactComponent as LogoIcon } from '@/assets/icons/logo.svg';
import { ReactComponent as MenuIcon } from '@/assets/icons/menu.svg';
import { ReactComponent as СrossIcon } from '@/assets/icons/add.svg';
import { ReactComponent as ExpandIcon } from '@/assets/icons/expand.svg';
import { ReactComponent as NotificationIcon } from '@/assets/icons/notification.svg';
import Container from '@/components/layout/container/Container';
import ButtonIcon from '@/components/shared/button/buttonIcon/ButtonIcon';
import Navigation from '@/components/common/navigation/Navigation';

import styles from './Header.module.scss';

const Header = () => {
  const [isShowMenu, setIsShowMenu] = useState(false);
  const { isMobile } = useMatchMedia();
  const headerRef = useRef(null);

  useClickAway(headerRef, () => {
    isMobile && isShowMenu && setIsShowMenu(false);
  });

  const handleMenu = () => {
    setIsShowMenu(!isShowMenu);
  };

  return (
    <header className={styles.header} ref={headerRef}>
      <Container>
        <div className={styles.wrapper}>
          {isMobile && (
            <button className={styles.menu} onClick={handleMenu}>
              {isShowMenu ? (
                <СrossIcon width={30} height={30} className={styles.cross} />
              ) : (
                <MenuIcon width={30} height={30} />
              )}
            </button>
          )}

          <LogoIcon height={35} />

          {!isMobile && <Navigation />}

          <div className={styles.widgets}>
            <div className={styles.notification}>
              FR
              <ButtonIcon icon={<NotificationIcon />} color="primary80" />
            </div>

            {!isMobile && (
              <>
                <ButtonIcon icon={'pm'} color="primary80" />

                <div className={styles.dropdown}>
                  <p className={styles.name}>PETER MORGAN</p>
                  <ExpandIcon width={24} height={24} />
                </div>
              </>
            )}
          </div>
        </div>
      </Container>

      {isMobile && (
        <Navigation isShow={isShowMenu} toggleMenu={setIsShowMenu} />
      )}
    </header>
  );
};

export default Header;
