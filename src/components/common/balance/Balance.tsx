import { ReactComponent as FlagIcon } from '@/assets/icons/flag.svg';
import Title from '@/components/shared/title/Title';
import Container from '@/components/layout/container/Container';

import styles from './Balance.module.scss';

const Balance = () => {
  return (
    <section>
      <Container>
        <Title hasMargin>XEPPT Account Balance</Title>
        <div className={styles.card}>
          <FlagIcon width={50} height={50} />
          <p className={styles.balance}>$191.00</p>
        </div>
      </Container>
    </section>
  );
};

export default Balance;
