import { ReactComponent as AddIcon } from '@/assets/icons/add.svg';
import Title from '@/components/shared/title/Title';
import ButtonIcon from '@/components/shared/button/buttonIcon/ButtonIcon';

import styles from './Bank.module.scss';

const Bank = () => {
  return (
    <section className={styles.section}>
      <Title hasMargin isBank>
        Bank Account and Cards
      </Title>
      <div className={styles.content}>
        <ul className={styles.cards}>
          <li className={styles.card}>Signature RBC visa ( ****3234)</li>
          <li className={styles.card}>TD Bank Debit ( ****0024)</li>
        </ul>

        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <a href="#" className={styles.link}>
          <ButtonIcon icon={<AddIcon />} color="secondary30" />
          Link a card or bank
        </a>
      </div>

      <div className={styles.add}>
        <button className={styles.button}>Add money</button>
      </div>
    </section>
  );
};

export default Bank;
