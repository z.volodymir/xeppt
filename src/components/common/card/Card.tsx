import { ReactComponent as LogoIcon } from '@/assets/icons/logo.svg';
import { ReactComponent as VisaIcon } from '@/assets/icons/visa.svg';

import styles from './Card.module.scss';

const Card = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.content}>
        <span className={styles.status}>Inactive</span>
        <p className={styles.balance}>
          <span className={styles.bill}>$0.00</span>
          Available Balance
        </p>
        <div className={styles.number}>4588 •••• •••• 0092</div>
      </div>
      <div className={styles.icons}>
        <LogoIcon width={80} className={styles.logo} />
        <VisaIcon width={50} className={styles.visa} />
      </div>
    </div>
  );
};

export default Card;
