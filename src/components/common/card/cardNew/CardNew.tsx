import { ReactComponent as AddIcon } from '@/assets/icons/add.svg';
import ButtonIcon from '@/components/shared/button/buttonIcon/ButtonIcon';

import styles from './CardNew.module.scss';

const CardNew = () => {
  return (
    <div className={styles.wrapper}>
      <ButtonIcon icon={<AddIcon />} color="secondary30" />
      <p className={styles.title}>Order a new card</p>
    </div>
  );
};

export default CardNew;
