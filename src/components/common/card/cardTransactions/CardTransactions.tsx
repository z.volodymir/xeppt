import { memo } from 'react';
import classNames from 'classnames';
import { ReactComponent as CardIcon } from '@/assets/icons/card.svg';
import { ReactComponent as WalletIcon } from '@/assets/icons/wallet.svg';
import ButtonIcon from '@/components/shared/button/buttonIcon/ButtonIcon';
import { Transaction } from '@/types';

import styles from './CardTransactions.module.scss';

interface Props extends Transaction {}

const CardTransactions = memo(
  ({ title, description, balance, isDebit, isXeppt }: Props) => {
    return (
      <div className={styles.wrapper}>
        {isXeppt ? (
          <ButtonIcon
            icon={<WalletIcon width={24} />}
            color="primary50"
            isTransaction
          />
        ) : (
          <ButtonIcon
            icon={<CardIcon width={24} />}
            color="primary80"
            isTransaction
          />
        )}
        <div className={styles.text}>
          <h3 className={styles.title}>{title}</h3>
          <p className={styles.description}>{description}</p>
        </div>
        <div
          className={classNames(styles.balance, {
            [styles.debit]: isDebit,
          })}
        >
          {balance}
        </div>
      </div>
    );
  }
);

export default CardTransactions;
