import { useEffect, useRef, useState } from 'react';
import classNames from 'classnames';
import CardTransactions from '@/components/common/card/cardTransactions/CardTransactions';
import { Transaction } from '@/types';

import styles from './TransactionsList.module.scss';

interface Props {
  filteredTransactions: Transaction[];
  transactionsCount: number;
  isChangeFilter: boolean;
}

const TransactionsList = ({
  filteredTransactions,
  transactionsCount,
  isChangeFilter,
}: Props) => {
  const [cardsHeight, setCardsHeight] = useState(0);
  const cardsRef = useRef<null | HTMLUListElement>(null);

  useEffect(() => {
    const refHeight = cardsRef.current?.scrollHeight;

    if (refHeight) {
      setCardsHeight(refHeight);
    }
  }, [transactionsCount]);

  return (
    <ul
      ref={cardsRef}
      className={styles.cards}
      style={
        cardsHeight
          ? { maxHeight: `${cardsHeight}px` }
          : { maxHeight: 'max-content' }
      }
    >
      {filteredTransactions.length &&
        filteredTransactions.map((transaction, index) => {
          return index >= transactionsCount ? null : (
            <li
              key={index}
              className={classNames(styles.card, {
                [styles.change]: isChangeFilter,
              })}
            >
              <CardTransactions
                title={transaction.title}
                description={transaction.description}
                balance={transaction.balance}
                isDebit={transaction.isDebit}
                isXeppt={transaction.isXeppt}
              />
            </li>
          );
        })}
    </ul>
  );
};

export default TransactionsList;
