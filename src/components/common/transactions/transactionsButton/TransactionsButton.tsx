import { Dispatch, SetStateAction } from 'react';
import classNames from 'classnames';
import { transactionsList } from '@/mocks/transactions';

import styles from './TransactionsButton.module.scss';

interface Props {
  isChangeFilter: boolean;
  setTransactionsCount: Dispatch<SetStateAction<number>>;
}

const TransactionsButton = ({
  isChangeFilter,
  setTransactionsCount,
}: Props) => {
  const showTransactions = () => {
    setTransactionsCount(transactionsList.length);
  };

  return (
    <div
      className={classNames(styles.more, {
        [styles.change]: isChangeFilter,
      })}
    >
      <button className={styles.button} onClick={showTransactions}>
        Show more
      </button>
    </div>
  );
};

export default TransactionsButton;
