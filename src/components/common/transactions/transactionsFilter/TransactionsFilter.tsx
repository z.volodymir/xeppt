import { Dispatch, SetStateAction, memo, useState } from 'react';
import ButtonFilter from '@/components/shared/button/buttonFilter/ButtonFilter';
import { transactionsList } from '@/mocks/transactions';
import { Transaction } from '@/types';

import styles from './TransactionsFilter.module.scss';

interface Props {
  setFilteredTransactions: Dispatch<SetStateAction<Transaction[]>>;
  setIsChangeFilter: Dispatch<SetStateAction<boolean>>;
}

const FilterTransactions = memo(
  ({ setFilteredTransactions, setIsChangeFilter }: Props) => {
    const [isActiveXeppt, setIsActiveXeppt] =
      useState<Transaction['isXeppt']>(false);

    const filterTransactions = (isXeppt: Transaction['isXeppt']) => {
      if (isXeppt) {
        setFilteredTransactions((prevTransactions) => {
          return prevTransactions.filter((transaction) => transaction.isXeppt);
        });
        setIsActiveXeppt(true);
      } else {
        setFilteredTransactions(transactionsList);
        setIsActiveXeppt(false);
      }

      setIsChangeFilter((prevFilter) => !prevFilter);
    };

    return (
      <div className={styles.buttons}>
        <ButtonFilter
          text="All"
          isActive={!isActiveXeppt}
          onClick={() => filterTransactions(false)}
        />
        <ButtonFilter
          text="XEPPT Card"
          isActive={isActiveXeppt}
          onClick={() => filterTransactions(true)}
        />
      </div>
    );
  }
);

export default FilterTransactions;
