import { useEffect, useState } from 'react';
import Title from '@/components/shared/title/Title';
import Container from '@/components/layout/container/Container';
import TransactionsList from '@/components/common/transactions/transactionsList/TransactionsList';
import TransactionsFilter from '@/components/common/transactions/transactionsFilter/TransactionsFilter';
import TransactionsButton from '@/components/common/transactions/transactionsButton/TransactionsButton';
import { transactionsList } from '@/mocks/transactions';

import styles from './Transactions.module.scss';

const initialCount = 3;

const Transactions = () => {
  const [filteredTransactions, setFilteredTransactions] =
    useState(transactionsList);
  const [transactionsCount, setTransactionsCount] = useState(initialCount);
  const [isChangeFilter, setIsChangeFilter] = useState(false);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => setIsChangeFilter(!isChangeFilter), [filteredTransactions]);

  return (
    <section>
      <Container>
        <Title hasMargin>Transactions</Title>
        <div className={styles.content}>
          <TransactionsFilter
            setFilteredTransactions={setFilteredTransactions}
            setIsChangeFilter={setIsChangeFilter}
          />

          <TransactionsList
            filteredTransactions={filteredTransactions}
            transactionsCount={transactionsCount}
            isChangeFilter={isChangeFilter}
          />

          {filteredTransactions.length > transactionsCount && (
            <TransactionsButton
              isChangeFilter={isChangeFilter}
              setTransactionsCount={setTransactionsCount}
            />
          )}
        </div>
      </Container>
    </section>
  );
};

export default Transactions;
