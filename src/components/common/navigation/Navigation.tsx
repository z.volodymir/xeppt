import { Dispatch, SetStateAction } from 'react';
import classNames from 'classnames';
import NavigationLink from './navigationLink/NavigationLink';

import styles from './Navigation.module.scss';

interface Props {
  isShow?: boolean;
  toggleMenu?: Dispatch<SetStateAction<boolean>>;
}

const Navigation = ({ isShow = false, toggleMenu = () => {} }: Props) => {
  return (
    <nav
      className={classNames(styles.wrapper, {
        [styles.show]: isShow,
      })}
    >
      <ul className={styles.list}>
        <li className={styles.item}>
          <NavigationLink
            url="."
            label="Home"
            onClick={() => toggleMenu(false)}
          />
        </li>
        <li className={styles.item}>
          <NavigationLink
            url="card"
            label="XEPPT Card"
            onClick={() => toggleMenu(false)}
          />
        </li>
        <li className={styles.item}>
          <NavigationLink
            url="statements"
            label="Statements"
            onClick={() => toggleMenu(false)}
          />
        </li>
      </ul>
    </nav>
  );
};

export default Navigation;
