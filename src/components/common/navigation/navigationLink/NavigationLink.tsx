import { NavLink } from 'react-router-dom';
import classNames from 'classnames';

import styles from './NavigationLink.module.scss';

interface Props {
  url: string;
  label: string;
  onClick: () => void;
}

const NavigationLink = ({ url, label, onClick }: Props) => {
  return (
    <NavLink
      onClick={onClick}
      className={({ isActive }) =>
        classNames(styles.link, {
          [styles.active]: isActive,
        })
      }
      to={url}
      end
    >
      {label}
    </NavLink>
  );
};

export default NavigationLink;
