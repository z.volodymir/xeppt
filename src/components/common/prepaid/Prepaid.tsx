import Title from '@/components/shared/title/Title';
import Container from '@/components/layout/container/Container';
import Card from '@/components/common/card/Card';
import CardNew from '@/components/common/card/cardNew/CardNew';

import styles from './Prepaid.module.scss';

const Prepaid = () => {
  return (
    <section className={styles.section}>
      <Container>
        <Title hasMargin>XEPPT Prepaid Card</Title>
      </Container>
      <ul className={styles.cards}>
        <li className={styles.card}>
          <Card />
        </li>
        <li className={styles.card}>
          <CardNew />
        </li>
      </ul>
    </section>
  );
};

export default Prepaid;
