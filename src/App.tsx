import { lazy } from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';

import HomePage from './pages/homePage/HomePage';
import Layout from './layouts/Layout';

const CardPage = lazy(() => import('./pages/CardPage'));
const StatementsPage = lazy(() => import('./pages/StatementsPage'));

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<HomePage />} />
          <Route path="/card" element={<CardPage />} />
          <Route path="/statements" element={<StatementsPage />} />
          <Route path="*" element={<Navigate to="." />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default App;
