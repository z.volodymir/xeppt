export type Transaction = {
  title: string;
  description: string;
  balance: string;
  isDebit: boolean;
  isXeppt: boolean;
};
