import Balance from '@/components/common/balance/Balance';
import Prepaid from '@/components/common/prepaid/Prepaid';
import Transactions from '@/components/common/transactions/Transactions';
import Bank from '@/components/common/bank/Bank';

import styles from './HomePage.module.scss';

const HomePage = () => {
  return (
    <div className={styles.wrapper}>
      <div className={styles.content}>
        <main className={styles.main}>
          <Balance />
          <Prepaid />
          <Transactions />
        </main>
        <aside className={styles.aside}>
          <Bank />
        </aside>
      </div>
    </div>
  );
};

export default HomePage;
