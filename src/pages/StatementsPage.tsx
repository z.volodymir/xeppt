import Container from '@/components/layout/container/Container';

const StatementsPage = () => {
  return (
    <Container>
      <h1>Statements Page</h1>
    </Container>
  );
};

export default StatementsPage;
