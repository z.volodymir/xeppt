import Container from '@/components/layout/container/Container';

const CardPage = () => {
  return (
    <Container>
      <h1>Card Page</h1>
    </Container>
  );
};

export default CardPage;
